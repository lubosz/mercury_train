import requests
import argparse
import json
from pathlib import Path


def get_or_fetch_assets(cache_path: Path) -> list[str]:
    asset_json_cache_path = cache_path / "indoor_assets.json"
    if not asset_json_cache_path.is_file():
        r = requests.get("https://api.polyhaven.com/assets?t=hdris&c=indoor")
        indoor_assets = r.json()
        with asset_json_cache_path.open("w") as f:
            f.write(json.dumps(indoor_assets))
        return list(indoor_assets.keys())
    else:
        with asset_json_cache_path.open("r") as f:
            return list(json.loads(f.read()).keys())


def get_or_fetch_urls(cache_path: Path, assets: list[str]) -> list[str]:
    url_json_cache_path = cache_path / "exr_urls.json"
    if not url_json_cache_path.is_file():
        print(f"Will retrieve URLs for {len(assets)} exr assets.")

        urls = []
        for i, name in enumerate(assets):
            print(f"{i+1}/{len(assets)} Getting url for {name}...")
            r = requests.get(f"https://api.polyhaven.com/files/{name}")
            urls.append(r.json()["hdri"]["4k"]["exr"]["url"])

        with url_json_cache_path.open("w") as f:
            f.write(json.dumps(urls))
        return urls
    else:
        with url_json_cache_path.open("r") as f:
            return json.loads(f.read())


def download_exrs(destination_path: Path, urls: list[str]):
    for i, url in enumerate(urls):
        file_name = url.rsplit("/")[-1]
        file_path = destination_path / file_name

        if not file_path.is_file():
            print(f"{i+1}/{len(urls)} Downloading {file_name}...")
            request = requests.get(url)
            with file_path.open("wb") as f:
                f.write(request.content)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--destination", type=str, default="exr",
                        help="EXR download destination")
    parser.add_argument("--cache", type=str, default="cache",
                        help="Directory containing cache files")
    args = parser.parse_args()

    # Create cache and download dirs
    destination_path = Path(args.destination)
    destination_path.mkdir(parents=True, exist_ok=True)

    cache_path = Path(args.cache)
    cache_path.mkdir(parents=True, exist_ok=True)

    assets = get_or_fetch_assets(cache_path)
    urls = get_or_fetch_urls(cache_path, assets)
    download_exrs(destination_path, urls)


if __name__ == "__main__":
    main()
