#include <iostream>
#include <random>
#include <opencv2/opencv.hpp>
#include <stereokit.h>

#include "aux/randoviz.hpp"
#include "xrt/xrt_defines.h"
#include "aux/pose_csv.hpp"

#include "pose_diversity_hand_maker.h"

extern const char *black_male_3dscanstore_proportions;

using namespace sk;

// DEBUG_GET_ONCE_FLOAT_OPTION(finger_pose_weight, "FP_WEIGHT", 0);

struct pgm_state
{

	bool going = false;
	struct fingerpose_creator *fingerpose_creator;
};

void
step(void *ptr)
{
	struct pgm_state &st = *(pgm_state *)ptr;

	if (sk::input_key(sk::key_f)) {
		st.going = true;
	}

	if (!st.going) {
		return;
	}

	TrajectorySample<26> out_sample;
	xrt_hand_joint_set joint_set;

	finger_pose_step(st.fingerpose_creator, &joint_set, &out_sample);

	sk::matrix fwd = matrix_t({0, 0, -0.3});

	sk::hierarchy_push(fwd);
	disp_xrt_hand(joint_set);
	// Whatever. This should be disabled sometimes but idk
	for (int i = 0; i < 26; i++) {
		draw_hand_axis_at_pose(sk_from_xrt(out_sample[i]), "a");
	}
	sk::hierarchy_pop();
}

void
shutdown(void *ptr)
{}

int
main(int argc, char *argv[])
{
	// u_trace_marker_init();

	struct pgm_state st;

	cJSON *J = cJSON_Parse(black_male_3dscanstore_proportions);

#if 0
	TrajectoryReader<26> tr(
	    "/3/epics/artificial_data_4/artificial_data_generator/data/finger_pose/training/live0.csv");
	st.fingerpose_creator = create_finger_pose(&tr, J, true, 1.0f / 60.0f);
#else
	st.fingerpose_creator = create_finger_pose(NULL, J, true, 1.0f / 60.0f);
#endif

	sk_settings_t settings = {};
	settings.app_name = "u-uhhhh 🧐🧐";
	settings.assets_folder = "/2/XR/sk-gradient-descent/Assets";
	// settings.display_preference = display_mode_flatscreen;
	settings.display_preference = display_mode_mixedreality;
	settings.flatscreen_width = 1920;
	settings.flatscreen_height = 1080;
	settings.overlay_app = true;
	settings.overlay_priority = 1;
	if (!sk_init(settings))
		return 1;
	// sk::render_set_ortho_size(10.5f);
	// sk::render_set_projection(sk::projection_ortho);
	sk::render_enable_skytex(false);

	sk_run_data(step, (void *)&st, shutdown, (void *)&st);

	return 0;
}
