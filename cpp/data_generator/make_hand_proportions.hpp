#include <iostream>
#include <random>

#include <opencv2/opencv.hpp>
#include <cjson/cJSON.h>

#include "kine_lm/lm_interface.hpp"

using namespace xrt::tracking::hand::mercury;

void
make_hand_proportions(const cJSON *proportions_json,
                      lm::hand_proportions &out_hand_proportions,
                      lm::HandLimit &out_hand_limit);
