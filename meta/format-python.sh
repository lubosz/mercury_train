#!/bin/sh
# Copyright 2019-2021, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0
# Author: Ryan Pavlik <ryan.pavlik@collabora.com>

# Formats all the source files in this project

set -e

AUTOPEP8=autopep8
if ! command -v ${AUTOPEP8} > /dev/null; then
        echo "${AUTOPEP8} not found, run 'python3 -m pip install autopep8'" 1>&2
        exit 1
fi
(
        ${AUTOPEP8} --version

        cd $(dirname $0)/..

        find \
                py/ \
                \( -name "*.py" \) \
                -exec ${AUTOPEP8} --in-place --aggressive --aggressive --max-line-length 120 \{\} +
)
