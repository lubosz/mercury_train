#!/bin/sh
# Copyright 2019, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0
# Author: Ryan Pavlik <ryan.pavlik@collabora.com>

# Runs "codespell" all the source files in this project.
# Pass:
#     -i 3
# as arguments to interactively fix detected issues,
# including ambiguous ones,
# rather than only directly fixing the ones it can.

# Success error code if no mistakes or only auto-fixable mistakes found.
# Failure error code if one or more ambiguous fixes found (requiring interactive fixing).

# See https://github.com/codespell-project/codespell
# or run pip3 install codespell

set -e

if ! command -v codespell > /dev/null; then
        echo "codespell not found, run 'python3 -m pip install codespell'" 1>&2
        exit 1
fi

# Comma-delimited list of words for codespell to not try to correct.
IGNORE_WORDS_LIST="ang,sinc,sie,inout,stoll,wil,daa,localy,od"

SCRIPTDIR=$(cd $(dirname $0) && pwd)

(
        cd $SCRIPTDIR/..
        find \
                *.md \
                doc \
                meta/format-*.sh \
                cpp/ \
                py/ \
                \( -name "*.c" \
                -o -name "*.cpp" \
                -o -name "*.h" \
                -o -name "*.hpp" \
                -o -name "*.sh" \
                -o -name "*.md" \
                -o -name "*.py" \
                -o -name "CMakeLists.txt" \) \
                -exec codespell \
                    --ignore-words-list="${IGNORE_WORDS_LIST}" \
                    -w \
                    "$@" \
                    \{\} +
)
