- Train a model that sees the last *three* frames, and predicts where the hand bounding box will be this frame, next frame and the one after that.

- Train another one that is given bounding boxes for one of the hands, and its job is to try to find the other hand. This would ideally be a lot faster to run, and be used while only one hand is tracked.

