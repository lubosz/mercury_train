'''
Stuff that will change per-machine.
'''
root = '/media/ryan/traindata-ryan'

hmdhandrects_location = f"{root}/HMDHandRects/"
egohands_convert = f"{root}/ego-hands/"


kitchens_images = f"{root}/EPIC-KITCHENS"
kitchens_annotations = f"{root}/kitchen_labels/"
kitchens_only_1st_sequence = True  # Set this to false if you're training a production model!

batch_size = 16
