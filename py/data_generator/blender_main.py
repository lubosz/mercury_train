import os
import pprint
import sys
import socket
from io import StringIO
import json
from pathlib import Path

import numpy as np
import pandas as pd
import math
import random
import mathutils
import bpy

# autopep8: off
sys.path.insert(0, os.path.dirname(__file__))
import mlib
from header import State
# autopep8: on


def add_lights(st: State):
    world = bpy.context.scene.world
    world.node_tree.nodes["Background"].inputs[1].default_value = (random.uniform(0.0, 1) ** 2) * 0.5
    world.node_tree.nodes["Background"].inputs[0].default_value = (1, 1, 1, 1)

    # num_lights = int(random.uniform(1, 6))
    num_lights = int(1 + 5 * (random.random() ** 2))
    # center = mathutils.Vector((0, 0.1, 0.3))
    center = np.array((0, 0.1, 0.3))
    print("num_lights", num_lights)
    for i in range(num_lights):
        obj = mlib.create_light(str(i))
        st.objects_to_delete.append(obj)
        max_light_dist = 2

        move_amount = np.random.uniform(-max_light_dist, max_light_dist, 3)
        square_move_len = np.linalg.norm(move_amount) ** 2
        # pos = center + square_move_len
        obj.location = center + mathutils.Vector((random.uniform(-max_light_dist, max_light_dist),
                                                  random.uniform(-max_light_dist, max_light_dist),
                                                  random.uniform(-max_light_dist, max_light_dist)))
        obj.data.energy = 10.0 + random.random() * 70.5
        obj.data.energy /= num_lights
        obj.data.energy *= square_move_len
        print(f"ENERGY {obj.data.energy} MOVE_LEN {square_move_len}")


def render_settings():
    # bpy.context.scene.eevee.taa_render_samples = 16
    bpy.context.scene.eevee.taa_render_samples = 1

    bpy.context.scene.cycles.adaptive_threshold = 0.1

    # Bloom adds seams between cubemap directions. It's bad.
    bpy.context.scene.eevee.use_bloom = False

    # 1% chance of no motion blur
    if random.random() > 0.01:
        bpy.context.scene.eevee.use_motion_blur = True

        # bias towards lower blur using square
        bpy.context.scene.eevee.motion_blur_shutter = random.uniform(0.0, 1.0) ** 2.3

        # bpy.context.scene.eevee.motion_blur_shutter = 1.0

        print("ADDING MOTION BLUR:", bpy.context.scene.eevee.motion_blur_shutter)

        bpy.context.scene.eevee.motion_blur_steps = 5
    else:
        print("NOT ADDING MOTION BLUR")


# Called to remove anything the artist made as an aid when
# rigging/creating the hand asset


def remove_artist_lights_and_cameras():
    for b_object in bpy.data.objects:
        do_delete = False
        do_delete = do_delete or b_object.type == "LIGHT"
        do_delete = do_delete or b_object.type == "CAMERA"
        if do_delete:
            bpy.data.objects.remove(b_object)


def load_blend_file(blend_path: Path):
    print(f"Loading blend model file `{blend_path.name}`...")

    blender_scene = bpy.data.scenes["Scene"]

    # bpy.ops.wm.open_mainfile(filepath=str(blend_path)), load_ui=False, use_scripts=True)
    old_collections = set(bpy.data.collections.keys())

    # filepath = root_dir + "/" + name + ".blend"
    with bpy.data.libraries.load(str(blend_path)) as (data_from, data_to):
        # This also worked but was jank because we didn't know which ones were orphan data or not
        # data_to.objects = data_from.objects

        # This will make another scene called "Scene.001"
        data_to.scenes = data_from.scenes

    # For anything that was in the scene collection from the file, append it to our scene.
    for obj in bpy.data.scenes["Scene.001"].collection.objects:
        blender_scene.collection.objects.link(obj)

    new_collections = set(bpy.data.collections.keys()) - old_collections
    print("Copying collections:", new_collections)

    # For anything that was in any other collection from the file, append it to our scene.
    for col in new_collections:
        for obj in bpy.data.collections[col].objects:
            blender_scene.collection.objects.link(obj)

    # (Don't do anything with orphan data from the file.)


# It's one of these two. I think it's rot2 but unsure.
rot1: mathutils.Matrix = mathutils.Matrix((
    (1, 0, 0, 0),
    (0, 0, -1, 0),
    (0, 1, 0, 0),
    (0, 0, 0, 1),
))

rot2: mathutils.Matrix = mathutils.Matrix((
    (1, 0, 0, 0),
    (0, 0, 1, 0),
    (0, -1, 0, 0),
    (0, 0, 0, 1),
))

# empirically rot1 seems to be the right one, based on what seems to work
# in get_finger_curls.cpp
rot = rot1


def hand_joints_bone_root(bones, bone):
    # bones.matrix_world is the matrix that "moves" points into the armature object's coordinate space.
    # bone.matrix is the matrix that moves points from the armature object's coordinate space to that bone's coordinate
    # space
    # Note that we used to do left_camera.matrix_world.inverse() @ ... in order to get things in the space of the left
    # camera as opposed to in the space of the world.
    # For now though we will do things in world space because we're not simulating a stereo camera anymore
    # TODO: for when we do validation datasets again
    m = bones.matrix_world @ bone.matrix @ rot
    return m.to_translation(), m.to_quaternion()


def hand_joints_bone_tip(bones, bone):
    # bones.matrix_world is the matrix that "moves" points into the armature object's coordinate space.
    # bone.matrix is the matrix that moves points from the armature object's coordinate space to that bone's coordinate
    # space
    # Note that we used to do left_camera.matrix_world.inverse() @ ... in order to get things in the space of the left
    # camera as opposed to in the space of the world.
    # For now though we will do things in world space because we're not simulating a stereo camera anymore
    # TODO: for when we do validation datasets again
    matrix_length = mathutils.Matrix()
    matrix_length.translation.y = bone.length
    m = bones.matrix_world @ bone.matrix @ matrix_length @ rot
    return m.to_translation(), m.to_quaternion()


def hand_joints(st, bones):
    # has a bunch of (position, orientation)s in it
    poses = [hand_joints_bone_root(bones, bones.pose.bones["Wrist"])]

    for finger in 'Thumb', 'Index', 'Middle', 'Ring', 'Little':
        if finger == 'Thumb':
            ls = 'Metacarpal', 'Proximal', 'Distal'
        else:
            ls = 'Metacarpal', 'Proximal', 'Intermediate', 'Distal'
        for joint in ls:
            name = finger + joint
            poses.append(hand_joints_bone_root(bones, bones.pose.bones[name]))

        # Get tip bone
        name = finger + 'Distal'

        # bone = bones.pose.bones[name]
        poses.append(hand_joints_bone_tip(bones, bones.pose.bones[name]))
    poses.append(hand_joints_bone_tip(bones, bones.pose.bones["Forearm"]))
    return poses


'''
# All of these shall have -z forward.
"metacarpal_roots": Array<Vec3>
"metacarpal_plus_z": Array<NormalizedVec3>
"metacarpal_plus_x": Array<NormalizedVec3>
"metacarpal_lengths": Array<float>
#"nonx_lengths": ["thumb": Array<float>, "index": Array<float>, ...]
"nonx_lengths": Array<Array<float>>
'''


def make_blender_to_openxr(l: mathutils.Vector):
    return [l[0], l[2], -l[1]]


def get_proportions(bones_object) -> dict:
    bobo = {"metacarpal_roots": [],
            "metacarpal_plus_x": [],
            "metacarpal_plus_z": [],
            "metacarpal_length": [],
            "metacarpal_min_max_x": [],
            "metacarpal_min_max_y": [],
            "metacarpal_min_max_z": [],
            "nonx_length": []}
    hand_size = bones_object.data.bones["Wrist"].length
    bobo["hand_size"] = float(hand_size)

    positions = []
    lengths = []

    for finger in 'Thumb', 'Index', 'Middle', 'Ring', 'Little':

        # note: data.head/data.tail seems to give parent-local
        # yarr this is true https://docs.blender.org/api/current/bpy.types.Bone.html#bpy.types.Bone.head
        # yarrrrr
        databone = bones_object.data.bones[finger + "Metacarpal"]
        posebone = bones_object.pose.bones[finger + "Metacarpal"]
        place = databone.head.copy()
        place2 = databone.tail

        # All the metacarpal bones are parented to the "wrist" _tip_, not the
        # root. Facepalm.
        place.y += hand_size

        # Needs to be a factor of the overall hand size
        place /= hand_size

        bobo["metacarpal_roots"].append(make_blender_to_openxr(place))
        bobo["metacarpal_plus_x"].append(make_blender_to_openxr(databone.x_axis))
        bobo["metacarpal_plus_z"].append(make_blender_to_openxr(-databone.y_axis))
        bobo["metacarpal_length"].append(databone.length / hand_size)

        # confirmed
        bobo["metacarpal_min_max_x"].append([posebone.ik_min_x, posebone.ik_max_x])
        bobo["metacarpal_min_max_y"].append([posebone.ik_min_z, posebone.ik_max_z])
        bobo["metacarpal_min_max_z"].append([posebone.ik_min_y, posebone.ik_max_y])

        positions.append(place)
        lengths.append((place - place2).length / hand_size)

        nonx_lengths = []

        if finger == 'Thumb':
            ls = 'Proximal', 'Distal'
        else:
            ls = 'Proximal', 'Intermediate', 'Distal'
        for joint in ls:
            place = bones_object.data.bones[finger + joint].head
            place2 = bones_object.data.bones[finger + joint].tail
            direction = place2 - place
            direction.x = 0

            nonx_lengths.append(direction.length / hand_size)

        bobo["nonx_length"].append(nonx_lengths)
    return bobo


def get_arm_length_mul(bones_object) -> float:
    upper = bones_object.data.bones["UpperArm"].length
    lower = bones_object.data.bones["Forearm"].length
    arm_len = upper + lower
    moses_arm_len = 0.5969

    mul = arm_len / moses_arm_len

    print("Multiplying mocap data by", mul)

    return mul

    # Moses arn is ~23.5 inches = 0.5969 meters


BUFSIZE = 1024 * 1024 * 1024


def send_message(message_dict: dict, server_port: int) -> dict:
    message_str: str = json.dumps(message_dict)
    message_bytes: bytes = message_str.encode('utf-8')

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect(("127.0.0.1", server_port))
    client_socket.sendall(message_bytes)

    # We have to do socket.MSG_WAITALL to make sure we receive all data the server sends us, not just "some".
    # As of 2023 this is a good case study of why you can't trust AI
    # assistants and have to think more deeply about things
    response_bytes: bytes = client_socket.recv(BUFSIZE, socket.MSG_WAITALL)
    client_socket.close()

    response_str: str = response_bytes.decode('utf-8')
    response_dict = json.loads(response_str)

    return response_dict


def before_start(st: State):
    # Auto-detection is annoying. Just make it right in the blend file.

    bones_object = bpy.data.objects["doot doot"]
    st.bones_object = bones_object

    st.arm_scale = get_arm_length_mul(bones_object)
    j = get_proportions(st.bones_object)
    st.hand_size = j["hand_size"]
    st.proportions_json = j


def get_wrist_pose_for_frame(wrist_poses: pd.DataFrame, frame_idx: int, elbow: bool = False) -> tuple:
    arr = wrist_poses.iloc[frame_idx]
    root = 1
    if elbow:
        # Size of a vec3+quaternion
        root += 7

    p = mathutils.Vector((arr[root], arr[root + 1], arr[root + 2]))

    q = mathutils.Quaternion()
    q.w = arr[root + 3]
    q.x = arr[root + 4]
    q.y = arr[root + 5]
    q.z = arr[root + 6]

    return p, q


def get_joint_pose_for_frame(finger_poses: pd.DataFrame, frame_idx: int, joint_idx: int) -> tuple:
    try:
        arr = finger_poses.iloc[frame_idx]
    except IndexError:
        raise Exception(f"Tried to get idx {frame_idx} but file is only {len(finger_poses)} long")
    root = 1 + (joint_idx * 7)
    # X is unchanged, Y is -Z, Z is Y
    p = mathutils.Vector((arr[root], arr[root + 1], arr[root + 2]))
    # p = mathutils.Vector((arr[root], -arr[root+2], arr[root+1]))

    q = mathutils.Quaternion()
    q.w = arr[root + 3]
    q.x = arr[root + 4]
    q.y = arr[root + 5]
    q.z = arr[root + 6]

    return p, q


def configure_sequence_render(st: State,
                              exr_dir_path: Path, num_frames: int,
                              sequence_out_path: Path,
                              use_exr_background: bool, use_alpha: bool,
                              wrist_pose_csv: str, finger_pose_csv: str):
    model_idx_file_path = sequence_out_path / "model_idx"
    with model_idx_file_path.open("w") as f:
        f.write(f"{st.model_idx}\n")

    blender_scene = bpy.data.scenes["Scene"]

    blender_scene.render.use_multiview = False
    blender_scene.render.views_format = 'STEREO_3D'
    blender_scene.render.fps = 30
    blender_scene.render.resolution_x = 768
    blender_scene.render.resolution_y = 768

    blender_scene.frame_start = 0
    blender_scene.frame_end = num_frames - 1

    remove_artist_lights_and_cameras()
    render_settings()

    mlib.add_ambient_occlusion(blender_scene)

    if use_exr_background:
        mlib.make_exr_background(blender_scene, exr_dir_path)
    else:
        add_lights(st)

    mlib.make_render_output(blender_scene, sequence_out_path)

    if use_alpha:
        mlib.make_render_alpha(blender_scene, sequence_out_path)

    tip_correct_rot = mathutils.Quaternion((-0.707107, 0.707107, 0, 0))
    rotate_finger_joints = mathutils.Quaternion((0.707107, 0.707107, 0, 0))

    wrist_empty = bpy.data.objects["xr_wrist_target"]
    wrist_empty.rotation_mode = "QUATERNION"  # yes this is necessary - nov 22

    elbow_empty = None
    if "simple_elbow_target" in bpy.data.objects:
        elbow_empty = bpy.data.objects["simple_elbow_target"]
        elbow_empty.rotation_mode = "QUATERNION"  # yes this is necessary - nov 22
        # TODO: This is never read
        st.orig_elbow_pos = elbow_empty.location.copy()

    # bpy.data.objects["xr_wrist_target"]
    palm_empty = mlib.create_empty("palmmmmm")
    st.objects_to_delete.append(palm_empty)
    palm_empty.parent = wrist_empty
    palm_empty.location.z = -0.04  # Doesn't matter?
    st.empties.append(palm_empty)

    # bpy.data.objects["xr_wrist_target"]
    fake_wrist_empty = mlib.create_empty("fakewrist")
    st.objects_to_delete.append(fake_wrist_empty)
    fake_wrist_empty.parent = wrist_empty
    st.empties.append(fake_wrist_empty)

    # bpy.data.objects["xr_wrist_target"]
    wrist_tail_target = mlib.create_empty("tail_target")
    st.objects_to_delete.append(wrist_tail_target)
    wrist_tail_target.parent = wrist_empty
    wrist_tail_target.location.z = -st.hand_size
    wrist_tail_target.rotation_quaternion = rotate_finger_joints

    wrist_parented_to_bone = mlib.create_empty("empty_parented_to_wrist")
    st.objects_to_delete.append(wrist_parented_to_bone)
    wrist_parented_to_bone.rotation_quaternion = rotate_finger_joints
    wrist_parented_to_bone.location.y -= st.hand_size
    wrist_parented_to_bone.parent = st.bones_object
    wrist_parented_to_bone.parent_bone = "Wrist"
    wrist_parented_to_bone.parent_type = "BONE"

    for finger in 'Thumb', 'Index', 'Middle', 'Ring', 'Little':
        if finger == 'Thumb':
            ls = 'Metacarpal', 'Proximal', 'Distal', "Tip"
        else:
            ls = "Metacarpal", 'Proximal', 'Intermediate', 'Distal', "Tip"
        empties = []
        for joint in ls:
            e = mlib.create_empty(finger + joint)
            st.objects_to_delete.append(e)
            e.empty_display_size = .01
            # e.empty_display_type = 'ARROWS'
            e.parent = wrist_parented_to_bone
            # e.parent_bone = "Wrist"
            # e.parent_type = "BONE"
            # e.parent_type = F"ARMATURE"
            empties.append(e)
            st.empties.append(e)
        for idx, joint in enumerate(ls[:-1]):
            e = empties[idx + 1]

            # we want metacarpal _and_ proximal to have chain count of 1.
            # proximal joint IK screws up accuracy in metacarpal IK

            # but not for the thumb.
            if finger == "Thumb":
                idx += 1
            con = mlib.new_constraint(st.bones_object, finger + joint, "IK")
            st.bone_constraints_to_delete.append(con)
            con.chain_count = max(1, idx)
            con.target = e

    # might be equivalent to above
    wrist_correct_prerot = mathutils.Quaternion((0.707107, 0.707107, 0, 0))

    wrist_poses: pd.DataFrame = pd.read_csv(StringIO(wrist_pose_csv))

    for i in range(num_frames):
        p, q = get_wrist_pose_for_frame(wrist_poses, i)
        wrist_empty.location.x = p.x
        wrist_empty.location.y = -p.z
        wrist_empty.location.z = p.y

        wrist_empty.location *= st.arm_scale

        q.rotate(wrist_correct_prerot)
        # wrist_empty.location = p
        wrist_empty.rotation_quaternion = q

        # wrist_empty.location.z = start_z + 0.02*math.sin(st.frame*0.1)
        # wrist_empty.location.x = start_x + 0.03*math.cos(st.frame*0.1)
        wrist_empty.keyframe_insert(data_path="location", frame=i)
        wrist_empty.keyframe_insert(data_path="rotation_quaternion", frame=i)

        p, q = get_wrist_pose_for_frame(wrist_poses, i, True)
        if elbow_empty:
            elbow_empty.location.x = p.x
            elbow_empty.location.y = -p.z
            elbow_empty.location.z = p.y

            # TODO: make this configurable by-model!
            elbow_empty.location *= st.arm_scale

            q.rotate(wrist_correct_prerot)
            elbow_empty.rotation_quaternion = q

            elbow_empty.keyframe_insert(data_path="location", frame=i)
            elbow_empty.keyframe_insert(data_path="rotation_quaternion", frame=i)

    finger_poses = pd.read_csv(StringIO(finger_pose_csv))

    for i in range(num_frames):
        for j in range(26):
            # Sigh, "just" making the target joints bigger makes it look a lot better (but it shouldn't)
            # s = readcsv_fingerpose.readcsv_fingerpose_settings(st.file, st.)
            # s = readcsv_fingerpose.readcsv_fingerpose_settings(st.file, 1.02)

            if j == 0:
                continue

            p, q = get_joint_pose_for_frame(finger_poses, i, j)

            # q = q.rotate()
            newguy = tip_correct_rot.copy()
            newguy.rotate(q)
            q = newguy
            # q = tip_correct_rot.rotate(q)
            e = st.empties[j]
            # eh?
            e.rotation_quaternion = q

            e.location = p
            # e.location[1] -= 0.095
            e.keyframe_insert(data_path="location", frame=i)
            e.keyframe_insert(data_path="rotation_quaternion", frame=i)

    # Ok, pose is made.

    # yeah, 26. we do have a forearm joint at the end, but we're skipping palm
    # as it is for nerds.
    hand_pose_flat = np.zeros((num_frames, 26 * 7))
    # For now, they're all valid. I _think_ we have none to exclude, for once!
    valid_samples = np.ones(num_frames, dtype=np.int32)
    # vec3, quat, fx, fy, cx, cy
    camera_info = np.zeros((num_frames, 3 + 4 + 4))

    arr_blender = []

    for i in range(num_frames):
        bpy.context.scene.frame_current = i

        bpy.context.view_layer.update()

        hand_joint_pose_array = hand_joints(st, st.bones_object)
        arr_blender.append([a[0] for a in hand_joint_pose_array])
        for j in range(26):  # 26th is the forearm tip
            root = j * 7
            # position
            hand_pose_flat[i, root + 0] = hand_joint_pose_array[j][0].x
            hand_pose_flat[i, root + 1] = hand_joint_pose_array[j][0].y
            hand_pose_flat[i, root + 2] = hand_joint_pose_array[j][0].z
            # orientation
            hand_pose_flat[i, root + 3] = hand_joint_pose_array[j][1].w
            hand_pose_flat[i, root + 4] = hand_joint_pose_array[j][1].x
            hand_pose_flat[i, root + 5] = hand_joint_pose_array[j][1].y
            hand_pose_flat[i, root + 6] = hand_joint_pose_array[j][1].z
        # arr_slice = np.asarray(hand_joint_pose_array)
        # arr_slice = arr_slice.flatten()
        # hand_pose_flat[i] = arr_slice

    # We write out the camera info later.

    df_hp = pd.DataFrame(hand_pose_flat)
    df_vs = pd.DataFrame(valid_samples, columns=["valid"])

    df_hp.to_csv(sequence_out_path / "hand_poses.csv", encoding='utf-8', index=False)  # , columns=columns);
    df_vs.to_csv(sequence_out_path / "valid_samples.csv", encoding='utf-8', index=False)  # , columns=columns);

    miniball_sphere = mlib.create_empty("mini_sphere")
    st.objects_to_delete.append(miniball_sphere)
    miniball_sphere.empty_display_size = 1.0
    miniball_sphere.empty_display_type = 'SPHERE'

    for i in range(num_frames):
        marker = blender_scene.timeline_markers.new(str(i))
        marker.frame = i
        camera = mlib.create_camera(f"camera{i}")
        st.objects_to_delete.append(camera)
        # camera.location.x = i/1000
        marker.camera = camera

        vec_list = arr_blender[i]
        center, radius = mlib.miniball(vec_list[:25])

        miniball_sphere.location = center
        miniball_sphere.scale = [radius] * 3
        miniball_sphere.keyframe_insert(data_path="scale", frame=i)
        miniball_sphere.keyframe_insert(data_path="location", frame=i)

        camera.rotation_quaternion = mlib.simple_rotation(mathutils.Vector((0, 0, -1)), center.normalized())
        camera.keyframe_insert(data_path="rotation_quaternion", frame=i)

        # A little padding
        target_radius = radius * 2.01

        angular_radius = math.sin(target_radius / center.length)

        camera.data.angle = angular_radius * 1.9

        camera.data.display_size = center.length * math.tan(camera.data.angle / 2) * 2
        camera.data.lens_unit = 'FOV'

        rr = blender_scene.render.resolution_x

        # If you look at the above, it looks like we might be able to just use angular_radius.
        # Don't! We add padding to that. Safest thing is to use half of the
        # actual angle used.
        tan_ = 1 / math.tan(camera.data.angle / 2)

        camera_info[i][0:3] = camera.location
        camera_info[i][3] = camera.rotation_quaternion.x
        camera_info[i][4] = camera.rotation_quaternion.y
        camera_info[i][5] = camera.rotation_quaternion.z
        camera_info[i][6] = camera.rotation_quaternion.w
        camera_info[i][7] = (rr / 2) * tan_  # fx
        camera_info[i][8] = (rr / 2) * tan_  # fy
        camera_info[i][9] = rr / 2  # cx
        camera_info[i][10] = rr / 2  # cy
        df_ci = pd.DataFrame(camera_info, columns=["px", "py", "pz",
                                                   "qx", "qy", "qz", "qw",
                                                   "fx", "fy",
                                                   "cx", "cy"])
        df_ci.to_csv(sequence_out_path / "camera_info.csv", encoding='utf-8', index=False)  # , columns=columns);


def clean_up_sequence_render(st: State):
    for obj in st.objects_to_delete:
        bpy.data.objects.remove(obj)
    # TODO: this is VERY questionable: we could just leave these around and only create them once
    for con in st.bone_constraints_to_delete:
        for bone in st.bones_object.pose.bones:
            try:
                bone.constraints.remove(con)
            except RuntimeError:
                pass
            except ReferenceError:
                # ReferenceError: StructRNA of type KinematicConstraint has
                # been removed
                pass
            # bones_object.constraints.remove(con)
    st.objects_to_delete.clear()
    st.bone_constraints_to_delete.clear()
    st.empties.clear()

    # for image in bpy.data.images:
    #     if image.users == 0:
    #         print(f"Found orphan image {image.name}!")
    #         bpy.data.images.remove(image)
    mlib.remove_orphans_of_datatype(bpy.data.images)
    mlib.remove_orphans_of_datatype(bpy.data.objects)


def main():
    st = State()
    server_port: int = int(os.getenv("SERVER_PORT"))
    st.slot_idx = int(os.getenv("SLOT_IDX"))
    st.model_idx = int(os.getenv("MODEL_IDX"))
    model_file = os.getenv("MODEL_FILE")
    exr_dir_path = Path(os.getenv("EXR_DIR_PATH"))

    assert exr_dir_path.is_dir()

    print("== Starting Blender ==")
    print(f"SLOT_IDX    {st.slot_idx}")
    print(f"MODEL_IDX   {st.model_idx}")
    print(f"SERVER_PORT {server_port}")
    print(f"MODEL_FILE  {model_file}")

    load_blend_file(Path(model_file))
    before_start(st)

    num_runs = 15

    for i in range(num_runs):
        print(f"== Starting render run ({i+1}/{num_runs}) ==")

        message = {"message_type": "normal", "slot_idx": st.slot_idx, "proportions": st.proportions_json}
        json_response = send_message(message, server_port)

        """
        print("GOT fingerpose_csv")
        pprint.pprint(reply["fingerpose_csv"])
        print("GOT wristpose_csv")
        pprint.pprint(reply["wristpose_csv"])
        """

        sequence_out_path = Path(json_response["output_color_images_folder"]).parent

        configure_sequence_render(st, exr_dir_path,
                                  json_response["num_frames"],
                                  sequence_out_path,
                                  json_response["use_exr_background"],
                                  json_response["render_alpha"],
                                  json_response["wristpose_csv"],
                                  json_response["fingerpose_csv"])

        if json_response["dont_render"]:
            break
        else:
            print(f"Rendering {json_response['num_frames']} frames...")
            bpy.ops.render.render(animation=True, write_still=True)
            clean_up_sequence_render(st)

    message = {"message_type": "goodbye", "slot_idx": st.slot_idx}
    send_message(message, server_port)
    print("Goodbye!")

    dont_exit_immediately = bool(int(os.getenv("DONT_EXIT_IMMEDIATELY")))
    if dont_exit_immediately:
        print("Not exiting because DONT_EXIT_IMMEDIATELY is set")

        bpy.ops.wm.save_as_mainfile(filepath="/tmp/tmp.blend")
        bpy.ops.wm.open_mainfile(filepath="/tmp/tmp.blend")
    else:
        print("Exiting because DONT_EXIT_IMMEDIATELY is not set")


if __name__ == "__main__":
    main()
