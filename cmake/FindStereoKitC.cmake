# Copyright 2019-2023 Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0
# Distributed under the Boost Software License, Version 1.0.
# (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)
#
# Original Author:
# 2019-2023 Ryan Pavlik <ryan.pavlik@collabora.com>

#.rst:
# FindStereoKitC
# ---------------
#
# Find the StereoKitC XR interaction/game engine library.
#
# Targets
# ^^^^^^^
#
# If successful, the following import target is created.
#
# ``StereoKitC::StereoKitC``
#
# Cache variables
# ^^^^^^^^^^^^^^^
#
# The following cache variable may also be set to assist/control the operation of this module:
#
# ``StereoKitC_ROOT_DIR``
#  The root to search for StereoKitC.

set(StereoKitC_ROOT_DIR
    "${StereoKitC_ROOT_DIR}"
    CACHE PATH "Root to search for StereoKitC"
	)

include(FindPackageHandleStandardArgs)

find_package(OpenXR QUIET)

# Check for CMake config first.
find_package(StereoKitC QUIET CONFIG)
if(StereoKitC_FOUND AND TARGET StereoKitC::StereoKitC)
	# Found config, let's prefer it.
	find_package_handle_standard_args(StereoKitC CONFIG_MODE)
	set(StereoKitC_LIBRARY StereoKitC::StereoKitC)
else()

	find_path(
		StereoKitC_INCLUDE_DIR
		NAMES stereokit.h
		PATHS ${StereoKitC_ROOT_DIR}
		PATH_SUFFIXES include StereoKitC
		)
	find_library(
		StereoKitC_LIBRARY
		NAMES StereoKitC libStereoKitC
		PATHS ${StereoKitC_ROOT_DIR} ${StereoKitC_ROOT_DIR}/build
		PATH_SUFFIXES lib
		)
	find_package_handle_standard_args(
		StereoKitC REQUIRED_VARS StereoKitC_LIBRARY StereoKitC_INCLUDE_DIR OpenXR_FOUND
		)
endif()
if(StereoKitC_FOUND)
	set(StereoKitC_INCLUDE_DIRS "${StereoKitC_INCLUDE_DIR}")
	set(StereoKitC_LIBRARIES "${StereoKitC_LIBRARY}")
	if(NOT TARGET StereoKitC::StereoKitC)
		add_library(StereoKitC::StereoKitC UNKNOWN IMPORTED)
		set_target_properties(
			StereoKitC::StereoKitC
			PROPERTIES
				IMPORTED_LOCATION "${StereoKitC_LIBRARY}"
				INTERFACE_INCLUDE_DIRECTORIES "${StereoKitC_INCLUDE_DIR}"
				IMPORTED_LINK_INTERFACE_LIBRARIES OpenXR::openxr_loader
			)
	endif()

	mark_as_advanced(StereoKitC_INCLUDE_DIR StereoKitC_LIBRARY)
endif()
mark_as_advanced(StereoKitC_ROOT_DIR)
